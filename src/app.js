const Joi = require('joi');
const express = require('express');
const { findAll, findOneById, findOneByName } = require('./actions');

const app = express();
app.use(express.json());

const users = findAll();

app.get('/', (req, res) => {
  const users = findAll();
  res.send('Hello Supreme QA Folks!');
});

app.get('/api/users', (req, res) => {
  const users = findAll();
  res.send(users);
});

app.get('/api/users/:id', (req, res) => {
  const users = findOneById(req.params.id);
  if (!users) {
    res.status(404).send('User not found');
  } else {
    res.send(users);
  }
});

app.post('/api/users', (req, res) => {
  const schema = Joi.object().keys({
    name: Joi.string().required(),
    team: Joi.string().required(),
  });

  const result = Joi.validate(req.body, schema);
  const user = {
    id: users[users.length - 1].id + 1,
    name: req.body.name,
    team: req.body.team,
  };

  if (result.error) res.status(404).send(result.error.details[0]);

  if (findOneByName(req.body.name)) {
    res.send(`User ${req.body.name} already exists`)
  } else {
    users.push(user);
    res.send(user);
  }
});

app.delete('/api/users/:id', (req, res) => {
  const user = findOneById(req.params.id);
  if (!user) {
    res.status(404).send(`User ${req.params.id} not found`);
  } else {
    const index = users.indexOf(user);
    users.splice(index, 1);
    res.send(`User ${req.params.id} has been deleted`);
  }
});

module.exports = app;

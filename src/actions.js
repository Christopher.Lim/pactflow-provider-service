const users = require('./data/users.json');

const findAll = () => {
    return users;
};

const findOneById = id => {
    return users.find(user => parseInt(id) === user.id);
};

const findOneByName = name => {
    return users.find(user => user.name === user.name);
}

const findOneByTeam = team => {
    return users.find(user => user.team === user.team);
}

module.exports = {
    findAll,
    findOneById,
    findOneByName,
    findOneByTeam
};

const { Verifier } = require('@pact-foundation/pact');
const project = require('../package.json');

describe("Pact Verification", () => {
    test("should validate the expectations of our consumer",  () => {
      const opts = {
        stateHandlers: {

        },
        provider: 'Pactflow-Provider',
        providerBaseUrl: "http://localhost:3000",
        pactBrokerUrl: process.env.PACT_BROKER_BASE_URL,
        pactBrokerToken: process.env.PACT_BROKER_TOKEN,
        publishVerificationResult: true,
        consumerVersionTags: [process.env.CI_COMMIT_REF_NAME || 'main'],
        providerVersion: project.version || '0.1.0',
        logLevel: "INFO",
        timeout: 30000
      };
  
      return new Verifier(opts).verifyProvider();
    });
});